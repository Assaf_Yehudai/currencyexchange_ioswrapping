import React from 'react';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';

import CurrencyRates from "./src/Modules/CurrencyRates/CurrencyRates";
import CurrencyStore from "./src/Common/Store/CurrencyRatesState";

const RNRates = () => {
    return (
        <Provider store={CurrencyStore}>
            <CurrencyRates/>
        </Provider>
   );
};

AppRegistry.registerComponent("RNRates", ()=>RNRates);

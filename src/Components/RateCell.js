import React from 'react';
import {View, Text} from 'react-native';
import {useSelector} from 'react-redux';


const RateCell = ({cellItem}) => {
    const baseCurrency = useSelector(state => state.baseCurrency)
    const { name, shortName, rate, country, symbol } = cellItem;
    const fixedRate = rate.toFixed(2);
    return (
        <View style={styles.container}>
            <View style={ styles.flagContainer }>
                <Text style={styles.flag}>{country.flag}</Text>
            </View>
            <View style={ styles.nameContainer}>
                <Text style={styles.currencyShortName}>{shortName}</Text>
                <Text style={styles.currencyName}>{name}</Text>
            </View>
            <View style={ styles.rateContainer }>
                <Text style={styles.rate}>{`${symbol} ${fixedRate}`}</Text>
                <Text style={styles.rateText}>{`1 ${baseCurrency} = ${fixedRate} ${shortName}`}</Text>
            </View>
        </View>
    );
};

export default RateCell;

const styles = {
    container: {
        flexDirection: 'row',
        height: 100,

        borderRadius: 10,
        borderColor: '#ABAAAA',
        borderWidth: 2,

        marginTop: 5,
        marginBottom: 5,
        marginRight: 8,
        marginLeft: 8,
    },

    currencyNameContainer: {
        flexDirection: 'column',
        height: '100%',
    },

    nameContainer: {
        height: '100%',
        width: '40%',
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },

    currencyShortName: {
        fontSize: 20,
        fontWeight: '700',
        color: '#000'
    },

    currencyName: {
        fontSize: 14,
        color: '#ABAAAA',
    },

    flagContainer: {
        height: '100%',
        width: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },

    flag: {
        fontSize: 40
    },

    rateContainer: {
        height: '100%',
        width: '40%',
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },

    rate: {
        fontSize: 17,
        fontWeight: 'bold',
        marginRight: 20
    },

    rateText: {
        marginTop: 5,
        marginRight: 20,
        fontSize: 11,
        color: '#ABAAAA'
    }
};

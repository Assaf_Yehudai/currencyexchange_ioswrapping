//
//  RatesComponent.swift
//  CurrencyExchange_iOSWrapping
//
//  Created by assaf yehudai on 24/01/2021.
//  Copyright © 2021 assaf yehudai. All rights reserved.
//

import UIKit
import React

class RatesComponent: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet var rnView: RCTRootView!
    
    // MARK: - Constructor
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        
        rnView = loadViewRNBridge()
        rnView.frame = bounds
        rnView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        rnView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(rnView)
    }
    
    private func loadViewRNBridge() -> RCTRootView? {
        
        guard let jsCodeLocation = URL(string: "http://localhost:8081/index.bundle?platform=ios") else
        { return nil}
        
        return RCTRootView( bundleURL: jsCodeLocation,
                            moduleName: "RNRates",
                            initialProperties: nil,
                            launchOptions: nil)
    }
    
    // MARK: - Public API
    
    func setBaseCurrency(currency: String) {
        
        RatesEventsEmitter.emitter.sendEvent(withName: "FetchRatesEventFromNative", body: currency)
    }
}

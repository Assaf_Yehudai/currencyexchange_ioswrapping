//
//  ViewController.swift
//  CurrencyExchange_iOSWrapping
//
//  Created by assaf yehudai on 24/01/2021.
//  Copyright © 2021 assaf yehudai. All rights reserved.
//

import UIKit
import React

class ViewController: UIViewController {

    @IBOutlet weak var ratesComponent: RatesComponent!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func onUSDTapped(_ sender: UIButton) {
        
        ratesComponent.setBaseCurrency(currency: "EUR")
    }
}
